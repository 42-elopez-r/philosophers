/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   did_someone_died.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/10 14:14:08 by elopez-r          #+#    #+#             */
/*   Updated: 2021/10/10 14:27:04 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philosophers.h>
#include <pthread.h>

bool	did_someone_died(struct s_death *death)
{
	bool	someone_died;

	pthread_mutex_lock(&(death->death_lock));
	someone_died = death->someone_died;
	pthread_mutex_unlock(&(death->death_lock));
	return (someone_died);
}
