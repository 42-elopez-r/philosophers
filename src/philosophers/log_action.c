/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   log_action.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/04 13:21:46 by elopez-r          #+#    #+#             */
/*   Updated: 2021/10/06 20:42:47 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philosophers.h>
#include <utils.h>
#include <pthread.h>
#include <stdlib.h>

/*
 * This function that totally doesn't emulate a global variable allows to set
 * the timestamp of the program start. If start_ms is NULL, just returns the
 * saved timestamp without modifying.
 */

t_ms	simulation_start(t_ms *start_ms)
{
	static t_ms	saved_start_ms;

	if (start_ms)
		saved_start_ms = *start_ms;
	return (saved_start_ms);
}

void	log_action(struct s_philosopher_data *philo, const char *action)
{
	char	*timestamp_str;
	char	*philo_id_str;

	timestamp_str = ft_ultoa(get_current_ms() - simulation_start(NULL));
	philo_id_str = ft_itoa(philo->id);
	pthread_mutex_lock(philo->stdout_lock);
	ft_putstr_fd(timestamp_str, 1);
	ft_putstr_fd(" ", 1);
	ft_putstr_fd(philo_id_str, 1);
	ft_putstr_fd(" ", 1);
	ft_putendl_fd(action, 1);
	pthread_mutex_unlock(philo->stdout_lock);
	free(timestamp_str);
	free(philo_id_str);
}
