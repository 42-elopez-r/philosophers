/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_philosophers.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/20 18:22:49 by elopez-r          #+#    #+#             */
/*   Updated: 2021/10/15 21:00:29 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philosophers.h>
#include <stdlib.h>
#include <pthread.h>

/*
 * This function allocates an array of struct s_fork with the passed ammount
 * of forks, initializing its peek mutex.
 * Returns NULL on error.
 */

static struct s_fork	*create_forks(unsigned int n_forks)
{
	struct s_fork	*forks;
	int				i;

	forks = malloc(n_forks * sizeof(struct s_fork));
	if (!forks)
		return (NULL);
	i = 0;
	while (i < (int)n_forks)
	{
		if (pthread_mutex_init(&(forks[i].peek_lock), NULL) != 0)
		{
			while (--i >= 0)
				pthread_mutex_destroy(&(forks[i].peek_lock));
			free(forks);
			return (NULL);
		}
		forks[i].in_use = false;
		i++;
	}
	return (forks);
}

static struct s_death	*create_death(void)
{
	struct s_death	*death;

	death = malloc(sizeof(struct s_death));
	if (!death)
		return (NULL);
	if (pthread_mutex_init(&(death->death_lock), NULL) != 0)
	{
		free(death);
		return (NULL);
	}
	death->someone_died = false;
	return (death);
}

/*
 * This function sets all fields of the philosophers array excepting thread and
 * ms_last_nom_nom, which are left uninitialized.
 */

static void	initialize_philosophers(struct s_philosopher_data *philos,
		struct s_args *args, struct s_fork *forks, struct s_death *death)
{
	unsigned int	i;

	i = 0;
	while (i < args->n_philos)
	{
		philos[i].id = i + 1;
		philos[i].forks = forks;
		philos[i].death = death;
		philos[i].args = *args;
		i++;
	}
}

static void	initialize_philosophers_and_the_secrets_chamber(
		struct s_philosopher_data *philos, pthread_mutex_t *stdout_lock)
{
	unsigned int	i;

	i = 0;
	while (i < philos->args.n_philos)
		philos[i++].stdout_lock = stdout_lock;
}

/*
 * This function allocates an array of struct s_philosopher_data according to
 * the received arguments. It will set all fields excepting thread and
 * ms_last_nom_nom, which are left uninitialized.
 * Returns NULL on error.
 */

struct s_philosopher_data	*create_philosophers(struct s_args *args)
{
	struct s_philosopher_data	*philos;
	struct s_fork				*forks;
	struct s_death				*death;
	pthread_mutex_t				*stdout_lock;

	death = create_death();
	stdout_lock = malloc(sizeof(pthread_mutex_t));
	forks = create_forks(args->n_philos);
	philos = malloc(args->n_philos * sizeof(struct s_philosopher_data));
	if (!death || !stdout_lock || !forks || !philos
		|| pthread_mutex_init(stdout_lock, NULL) != 0)
	{
		delete_death(death);
		free(stdout_lock);
		delete_forks(forks, args->n_philos);
		return (NULL);
	}
	initialize_philosophers(philos, args, forks, death);
	initialize_philosophers_and_the_secrets_chamber(philos, stdout_lock);
	return (philos);
}
