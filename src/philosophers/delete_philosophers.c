/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   delete_philosophers.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/02 16:50:49 by elopez-r          #+#    #+#             */
/*   Updated: 2021/10/16 15:54:40 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philosophers.h>
#include <stdlib.h>
#include <pthread.h>

void	delete_forks(struct s_fork *forks, unsigned int n_forks)
{
	unsigned int	i;

	if (!forks)
		return ;
	i = 0;
	while (i < n_forks)
		pthread_mutex_destroy(&(forks[i++].peek_lock));
	free(forks);
}

void	delete_death(struct s_death *death)
{
	pthread_mutex_destroy(&(death->death_lock));
	free(death);
}

void	delete_philosophers(struct s_philosopher_data *philos)
{
	delete_death(philos[0].death);
	delete_forks(philos[0].forks, philos[0].args.n_philos);
	pthread_mutex_destroy(philos[0].stdout_lock);
	free(philos[0].stdout_lock);
	free(philos);
}
