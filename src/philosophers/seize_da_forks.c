/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   seize_da_forks.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/04 11:32:26 by elopez-r          #+#    #+#             */
/*   Updated: 2021/10/14 12:22:24 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philosophers.h>
#include <pthread.h>

/*
 * This function checks if the forks of the given philosopher are unused, and
 * in such case it flags them as used returning true.
 */

bool	seize_da_forks(struct s_philosopher_data *philo)
{
	unsigned int	fork_ids[2];
	bool			seized;

	get_forks_index_for_philo(fork_ids, philo);
	if (fork_ids[0] == fork_ids[1])
		return (false);
	pthread_mutex_lock(&(philo->forks[fork_ids[0]].peek_lock));
	pthread_mutex_lock(&(philo->forks[fork_ids[1]].peek_lock));
	if (!philo->forks[fork_ids[0]].in_use && !philo->forks[fork_ids[1]].in_use)
	{
		philo->forks[fork_ids[0]].in_use = true;
		philo->forks[fork_ids[1]].in_use = true;
		seized = true;
		log_action(philo, "has taken a fork");
		log_action(philo, "has taken a fork");
	}
	else
		seized = false;
	pthread_mutex_unlock(&(philo->forks[fork_ids[0]].peek_lock));
	pthread_mutex_unlock(&(philo->forks[fork_ids[1]].peek_lock));
	return (seized);
}
