/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ms_sleep.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/02 18:00:09 by elopez-r          #+#    #+#             */
/*   Updated: 2021/10/06 21:10:57 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philosophers.h>
#include <utils.h>
#include <unistd.h>
#include <sys/time.h>

void	ms_sleep(t_ms ms)
{
	const t_ms	us_step = 500;
	t_ms		end;

	end = get_current_ms() + ms;
	while (get_current_ms() < end)
		usleep(us_step);
}
