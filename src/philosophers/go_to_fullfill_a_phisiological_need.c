/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   go_to_fullfill_a_phisiological_need.c              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/06 19:50:37 by elopez-r          #+#    #+#             */
/*   Updated: 2021/10/16 14:26:36 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philosophers.h>

static void	check_if_it_died(struct s_philosopher_data *philo,
		t_ms time_to_do_it, t_ms time_to_actually_do_it)
{
	if (time_to_actually_do_it != time_to_do_it)
	{
		pthread_mutex_lock(&(philo->death->death_lock));
		if (!philo->death->someone_died)
		{
			philo->death->someone_died = true;
			log_action(philo, "died");
		}
		pthread_mutex_unlock(&(philo->death->death_lock));
	}
}

void	go_to_fullfill_a_phisiological_need(struct s_philosopher_data *philo,
		const char *phisiological_need, unsigned long time_to_do_it)
{
	t_ms	time_to_actually_do_it;
	t_ms	now;
	bool	leave;

	leave = false;
	now = get_current_ms();
	if (now + time_to_do_it
		> philo->ms_last_nom_nom + philo->args.time_to_die)
	{
		time_to_actually_do_it = (philo->ms_last_nom_nom
				+ philo->args.time_to_die) - now;
	}
	else
		time_to_actually_do_it = time_to_do_it;
	pthread_mutex_lock(&(philo->death->death_lock));
	if (philo->death->someone_died)
		leave = true;
	else
		log_action(philo, phisiological_need);
	pthread_mutex_unlock(&(philo->death->death_lock));
	if (leave)
		return ;
	ms_sleep(time_to_actually_do_it);
	check_if_it_died(philo, time_to_do_it, time_to_actually_do_it);
}
