/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drop_da_fork.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/04 11:32:26 by elopez-r          #+#    #+#             */
/*   Updated: 2021/10/06 20:17:23 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philosophers.h>
#include <pthread.h>

/*
 * This function marks the forks of the philosopher as unused.
 */

void	drop_da_forks(struct s_philosopher_data *philo)
{
	unsigned int	fork_ids[2];

	get_forks_index_for_philo(fork_ids, philo);
	pthread_mutex_lock(&(philo->forks[fork_ids[0]].peek_lock));
	pthread_mutex_lock(&(philo->forks[fork_ids[1]].peek_lock));
	philo->forks[fork_ids[0]].in_use = false;
	philo->forks[fork_ids[1]].in_use = false;
	pthread_mutex_unlock(&(philo->forks[fork_ids[0]].peek_lock));
	pthread_mutex_unlock(&(philo->forks[fork_ids[1]].peek_lock));
}
