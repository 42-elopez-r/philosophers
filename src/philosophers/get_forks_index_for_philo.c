/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_forks_index_for_philo.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/04 12:50:31 by elopez-r          #+#    #+#             */
/*   Updated: 2021/10/15 21:01:28 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philosophers.h>

/*
 * This function stores the corresponding forks index of the given philosopher
 * into the fork_indx array.
 */

void	get_forks_index_for_philo(unsigned int fork_indx[2],
		struct s_philosopher_data *philo)
{
	if (philo->id == 1)
	{
		fork_indx[0] = philo->args.n_philos - 1;
		fork_indx[1] = 0;
	}
	else
	{
		fork_indx[0] = philo->id - 2;
		fork_indx[1] = philo->id - 1;
	}
}
