/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   circle_of_life.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/02 21:58:41 by elopez-r          #+#    #+#             */
/*   Updated: 2021/10/16 17:16:44 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philosophers.h>

static t_ms	get_time_left_alive(struct s_philosopher_data *philo)
{
	return (philo->ms_last_nom_nom + philo->args.time_to_die
		- get_current_ms());
}

static bool	think(struct s_philosopher_data *philo)
{
	log_action(philo, "is thinking");
	while (!seize_da_forks(philo))
	{
		if (did_someone_died(philo->death))
			return (true);
		if (get_time_left_alive(philo) > philo->args.time_to_die * 0.3)
			ms_sleep(philo->args.time_to_die * 0.012);
		if (philo->ms_last_nom_nom + philo->args.time_to_die
			<= get_current_ms())
		{
			pthread_mutex_lock(&(philo->death->death_lock));
			if (!philo->death->someone_died)
			{
				philo->death->someone_died = true;
				log_action(philo, "died");
			}
			pthread_mutex_unlock(&(philo->death->death_lock));
			return (true);
		}
	}
	return (false);
}

void	circle_of_life(struct s_philosopher_data *philo)
{
	philo->ms_last_nom_nom = get_current_ms();
	while (!did_someone_died(philo->death) && philo->args.n_eatings--)
	{
		if (think(philo))
			return ;
		philo->ms_last_nom_nom = get_current_ms();
		go_to_fullfill_a_phisiological_need(philo, "is eating",
			philo->args.time_to_eat);
		drop_da_forks(philo);
		if (did_someone_died(philo->death))
			return ;
		go_to_fullfill_a_phisiological_need(philo, "is sleeping",
			philo->args.time_to_sleep);
	}
}
