/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_args.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/31 21:14:34 by elopez-r          #+#    #+#             */
/*   Updated: 2021/09/12 20:03:12 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <args.h>
#include <utils.h>

static bool	check_args(int argc, char *argv[])
{
	int	i;

	if (argc != 5 && argc != 6)
	{
		ft_putendl_fd("Wrong number of arguments", 2);
		return (false);
	}
	i = 1;
	while (i < argc)
	{
		if (!is_natural_number(argv[i]))
		{
			ft_putstr_fd("Not a natural number: ", 2);
			ft_putendl_fd(argv[i], 2);
			return (false);
		}
		i++;
	}
	return (true);
}

bool	parse_args(int argc, char *argv[], struct s_args *args)
{
	if (!check_args(argc, argv))
		return (false);
	args->n_philos = ft_atoi(argv[1]);
	args->time_to_die = ft_atol(argv[2]);
	args->time_to_eat = ft_atol(argv[3]);
	args->time_to_sleep = ft_atol(argv[4]);
	if (argc == 6)
		args->n_eatings = ft_atoi(argv[5]);
	else
		args->n_eatings = -1;
	return (true);
}
