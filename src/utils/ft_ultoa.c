/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 14:47:59 by elopez-r          #+#    #+#             */
/*   Updated: 2021/10/06 20:51:32 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <utils.h>
#include <stdlib.h>

static int	count_chars(unsigned long n)
{
	int		chars;

	if (n > 0)
		chars = 0;
	else
		return (1);
	while (n > 0)
	{
		chars++;
		n /= 10;
	}
	return (chars);
}

char	*ft_ultoa(unsigned long n)
{
	char	*str;
	long	n_l;
	int		len;

	len = count_chars(n);
	str = NULL;
	str = malloc(len + 1);
	if (str)
	{
		str[len] = '\0';
		len--;
		n_l = n;
		while (n_l >= 10)
		{
			str[len--] = n_l % 10 + '0';
			n_l /= 10;
		}
		str[len] = n_l + '0';
	}
	return (str);
}
