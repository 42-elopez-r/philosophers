/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_natural_number.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/31 21:07:50 by elopez-r          #+#    #+#             */
/*   Updated: 2021/09/12 19:53:48 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <utils.h>
#include <stddef.h>

bool	is_natural_number(const char *str)
{
	size_t	i;
	bool	not_empty;

	not_empty = false;
	i = 0;
	while (str[i])
	{
		if (str[i] >= '0' && str[i] <= '9')
			not_empty = true;
		else
			return (false);
		i++;
	}
	return (not_empty);
}
