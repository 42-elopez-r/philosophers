/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/07 15:42:03 by elopez-r          #+#    #+#             */
/*   Updated: 2021/09/12 20:02:09 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <utils.h>

static int	is_space(char c)
{
	return (c == ' ' || c == '\t' || c == '\n' || c == '\f'
		|| c == '\v' || c == '\r');
}

long	ft_atol(const char *nptr)
{
	long long	acm;
	int			sign;

	while (*nptr && is_space(*nptr))
		nptr++;
	if (*nptr == '+' || *nptr == '-')
	{
		if (*nptr == '+')
			sign = 1;
		else
			sign = -1;
		nptr++;
	}
	else
		sign = 1;
	acm = 0;
	while (*nptr >= '0' && *nptr <= '9')
	{
		acm = acm * 10 + (*nptr - '0');
		nptr++;
	}
	acm *= sign;
	return (acm);
}
