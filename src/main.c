/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/20 16:38:10 by elopez-r          #+#    #+#             */
/*   Updated: 2021/10/14 12:22:50 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <args.h>
#include <philosophers.h>
#include <utils.h>
#include <pthread.h>

static void	*circle_of_life_wrapper(void *philo)
{
	circle_of_life(philo);
	return (NULL);
}

static void	its_thinking_time(struct s_args *args)
{
	struct s_philosopher_data	*philos;
	unsigned int				i;
	t_ms						start_ms;

	philos = create_philosophers(args);
	if (!philos)
	{
		ft_putendl_fd("Can't create philosophers", 2);
		return ;
	}
	start_ms = get_current_ms();
	simulation_start(&start_ms);
	i = 0;
	while (i < args->n_philos)
	{
		pthread_create(&(philos[i].thread), NULL, circle_of_life_wrapper,
			philos + i);
		i++;
	}
	i = 0;
	while (i < args->n_philos)
		pthread_join(philos[i++].thread, NULL);
	delete_philosophers(philos);
}

int	main(int argc, char *argv[])
{
	struct s_args	args;

	if (!parse_args(argc, argv, &args))
		return (1);
	if (args.n_philos < 1)
	{
		ft_putendl_fd("At least one philosopher required", 2);
		return (1);
	}
	its_thinking_time(&args);
	return (0);
}
