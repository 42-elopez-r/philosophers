CC = gcc
SHELL = /bin/sh
CFLAGS += -I include -Wall -Werror -Wextra
LDFLAGS += -lpthread

NAME = philo

SRCS:= $(shell find src -name "*.c")
OBJS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $(CFLAGS) -o $(NAME) $(OBJS) $(LDFLAGS)

.o: .c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	find . -name "*.o" -delete -print

fclean: clean
	rm -f $(NAME)

re: fclean all

bonus: all

debug: CFLAGS = -I include -Wall -Werror -g
debug: fclean $(NAME)

.PHONY: all clean fclean re debug
