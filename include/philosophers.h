/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/15 19:05:45 by elopez-r          #+#    #+#             */
/*   Updated: 2021/10/15 21:20:46 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILOSOPHERS_H
# define PHILOSOPHERS_H

# include <args.h>
# include <pthread.h>
# include <stdbool.h>

typedef unsigned long	t_ms;

struct s_fork
{
	pthread_mutex_t			peek_lock;
	bool					in_use;
};

struct s_death
{
	pthread_mutex_t			death_lock;
	bool					someone_died;
};

struct s_philosopher_data
{
	unsigned int			id;
	pthread_t				thread;
	struct s_fork			*forks;
	t_ms					ms_last_nom_nom;
	struct s_death			*death;
	pthread_mutex_t			*stdout_lock;
	struct s_args			args;
};

t_ms						get_current_ms(void);

struct s_philosopher_data	*create_philosophers(struct s_args *args);

void						delete_philosophers(
								struct s_philosopher_data *philos);

void						delete_forks(struct s_fork *forks,
								unsigned int n_forks);

void						delete_death(struct s_death *death);

void						ms_sleep(t_ms ms);

void						circle_of_life(struct s_philosopher_data *philo);

t_ms						simulation_start(t_ms *start_ms);
void						log_action(struct s_philosopher_data *philo,
								const char *action);

void						get_forks_index_for_philo(unsigned int fork_indx[2],
								struct s_philosopher_data *philo);

bool						seize_da_forks(struct s_philosopher_data *philo);
void						drop_da_forks(struct s_philosopher_data *philo);

bool						did_someone_died(struct s_death *death);

void						go_to_fullfill_a_phisiological_need(
								struct s_philosopher_data *philo,
								const char *phisiological_need,
								unsigned long time_to_do_it);

#endif
